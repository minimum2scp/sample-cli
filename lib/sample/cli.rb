require "sample/cli/version"

module Sample
  module Cli
    autoload :Command, 'sample/cli/command'
    autoload :Base,    'sample/cli/base'
    autoload :SubCmd1, 'sample/cli/sub_cmd1'
    autoload :SubCmd2, 'sample/cli/sub_cmd2'
  end
end
