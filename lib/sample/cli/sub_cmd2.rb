require 'thor'

module Sample
  module Cli
    class SubCmd2 < Base
      desc "noguemon", "puts noguemon"
      def noguemon
        logger.info "noguemon called"
        puts "noguemon"
      end
    end
  end
end

