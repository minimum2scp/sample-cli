require 'thor'
require 'logger'

module Sample
  module Cli
    class Base < Thor
      class_option :log,       type: :string, default: '-'
      class_option :log_level, type: :string, default: 'info'

      no_commands do
        def logger
          unless @logger
            if options[:log] == '-'
              @logger = Logger.new($stdout)
            else
              @logger = Logger.new(options[:log])
            end
            @logger.level = Logger.const_get(options[:log_level].to_s.upcase)
          end
          @logger
        end
      end
    end
  end
end

