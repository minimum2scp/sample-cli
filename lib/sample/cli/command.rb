require 'thor'

module Sample
  module Cli
    class Command < Base
      {
        "sub_cmd1" => "SubCmd1",
        "sub_cmd2" => "SubCmd2",
      }.each do |name, classname|
        desc "#{name} ...ARGS", "description of #{name} here..."
        subcommand name, ::Sample::Cli.const_get(classname)
      end
    end
  end
end

